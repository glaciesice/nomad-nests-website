// https://nuxt.com/docs/api/configuration/nuxt-config
export default {
  devtools: { enabled: true },
  css: ['@/assets/fontLoader.css'],
  modules: [
    '@nuxtjs/tailwindcss',
    '@nuxtjs/robots',
    'nuxt-icon',
  ],
  robots: {
    UserAgent: '*',
    Disallow: '/'
  }
}